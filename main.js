console.log("Display numbers from 1 to 1000.  All even numbers will be shown in bold.  And all multiples of 3 will be underlined")




for (let num = 1; num <= 1000; num++){
    let rooted = Math.sqrt(num)
    if(num % 2==0){
        document.write("<b>" + num + "</b> ");
    }
    else if(num % 3==0){
        document.write("<u>" + num + "</u> ");
    }
    else if(Number.isInteger(rooted) == false){
        document.write("<canvas width=\"5\" height=\"8\" style=\"border:1px solid black;\">" + num + "</canvas> " )
    }
    else{
        document.write(num + " ")
    }
}